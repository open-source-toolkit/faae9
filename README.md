# Spring Cloud Gateway 整合 Spring Security 统一登录认证鉴权

## 项目简介

本项目是一个基于Spring Cloud Gateway的微服务框架，重点整合了Spring Security，实现了统一的登录认证和鉴权功能。微服务间通过Redis来获取登录用户的信息，确保了用户身份的统一管理和安全访问。

## 主要功能

1. **Spring Cloud Gateway集成**：利用Spring Cloud Gateway作为微服务网关，提供统一的入口管理和路由功能。
2. **Spring Security整合**：通过Spring Security实现用户认证和授权，确保微服务的安全性。
3. **Redis用户信息管理**：微服务间通过Redis共享用户登录信息，实现用户身份的统一管理。

## 使用说明

1. **环境准备**：
   - 确保已安装Java开发环境（JDK 8+）。
   - 配置好Redis服务，并确保微服务能够连接到Redis。

2. **项目导入**：
   - 将项目导入到IDE中，如IntelliJ IDEA或Eclipse。
   - 配置项目的依赖项，确保所有必要的库都已正确加载。

3. **配置文件**：
   - 根据实际环境配置`application.yml`或`application.properties`文件，设置数据库连接、Redis连接等信息。

4. **启动项目**：
   - 启动Spring Cloud Gateway服务，确保网关能够正常运行。
   - 启动各个微服务，确保它们能够通过网关进行访问。

5. **测试**：
   - 使用Postman或其他API测试工具，测试登录认证和鉴权功能。
   - 确保用户登录后，微服务能够正确获取并使用Redis中的用户信息。

## 注意事项

- 确保Redis服务稳定运行，避免因Redis故障导致用户信息丢失。
- 定期更新Spring Security和Spring Cloud Gateway的版本，以获取最新的安全补丁和功能改进。

## 贡献

欢迎大家提出改进建议或提交代码，共同完善这个项目。

## 许可证

本项目采用MIT许可证，详情请参阅`LICENSE`文件。